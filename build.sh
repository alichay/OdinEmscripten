#!/bin/bash
echo "EMSCRIPTEN::false;" > build_dir/build_config.odin
mkdir bin > /dev/null 2>&1
if [[ `uname` == "MSYS"* ]]; then
	rm bin/game.exe
	cmd /c "..\\Odin\\misc\\shell.bat & ..\\Odin\\odin build build_dir/game.odin"
	mv build_dir/game.exe bin/game.exe
	cp win32libs/*.dll bin/
	rm build_dir/game.bc
	rm build_dir/game.ll
else
	rm bin/game
	../Odin/odin build build_dir/game.odin
	mv build_dir/game bin/game
fi

# Move libraries
if [ "$(uname)" == "Darwin" ]; then
	# None for OSX yet!
	echo
elif [ "$(uname)" == "Linux" ]; then
	mv linuxlibs/* bin/
fi