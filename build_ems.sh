#!/bin/sh

# Do not include the .odin here
in_path="build_dir/game"
# Likewise, no .html here
out="game"
#asset_dir="runenv"
config="-s USE_SDL=2 -lGLESv2 -lEGL"

asset_dir_prefix=""
startdir=`pwd`
echo "Copying and patching source code"
rm -rf /tmp/odin_emscripten_build
rm -rf js_out
mkdir /tmp/odin_emscripten_build
cp -r . /tmp/odin_emscripten_build
pushd /tmp/odin_emscripten_build
for fn in `find . -name "*.odin" -type f`; do
	echo "Patching $fn"
	echo "sed -i 's/EMSCRIPTEN[[:space:]]*?::[[:space:]]*?false/EMSCRIPTEN::true/g' $fn"
	sed -i 's/EMSCRIPTEN[[:space:]]*::[[:space:]]*false/EMSCRIPTEN::true/mg' $fn
done
echo EMSCRIPTEN::true; > build_dir/build_config.odin
if [[ ! -z "$asset_dir" ]]; then asset_dir_prefix="--preload-file"; fi
echo Compiling with Odin.
echo "Since the compiler automatically tries to link, you'll see a linker error."
echo This is expected.
echo ------------------------
echo "> \"${startdir}/../OdinOfficial/odin\" build ${in_path}.odin"
"${startdir}/../OdinOfficial/odin" build "${in_path}.odin"
echo ------------------------
echo "Patching the LL file (LLVM intermediary form)"
python2.7 emscripten_things/fix_ll.py "${in_path}.ll"
echo "Now we're going to compile the .js file."
echo ------------------------
echo "> emcc \"${in_path}.ll\" $@ $config --js-library emscripten_things/odin.js -o \"${out}.html\" $asset_dir_prefix $asset_dir"
emcc "${in_path}.ll" $@ $config --js-library emscripten_things/odin.js -o "${out}.html" $asset_dir_prefix $asset_dir
echo ------------------------
popd
mkdir js_out
cp -r $(echo "/tmp/odin_emscripten_build/${out}.*") js_out/