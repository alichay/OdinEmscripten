#foreign_system_library ovr "win32libs/openvr_api.lib" when (ODIN_OS == "windows");
#foreign_system_library ovr "openvr_api"               when (ODIN_OS == "osx" || ODIN_OS == "linux");

// Most of the constants I just modified by hand.

// For the enums, I created this simple Sublime regex.
//   typedef enum (.*?)\n{\n([\s\S]*?)\n} \1;
// to
//   \1 :: enum {
//   \2
//   }"

// Structs are converted by-hand on a need-to-use basis.

PropertyContainerHandle :: u64;
PropertyTypeTag :: u32;

// OpenVR Constants

k_unMaxDriverDebugResponseSize                      : uint   : 32768; // unsigned int
k_unTrackedDeviceIndex_Hmd                          : uint   : 0; // unsigned int
k_unMaxTrackedDeviceCount                           : uint   : 16; // unsigned int
k_unTrackedDeviceIndexOther                         : uint   : 4294967294; // unsigned int
k_unTrackedDeviceIndexInvalid                       : uint   : 4294967295; // unsigned int
k_ulInvalidPropertyContainer                        : u64    : 0; // unsigned long
k_unInvalidPropertyTag                              : uint   : 0; // unsigned int
k_unFloatPropertyTag                                : uint   : 1; // unsigned int
k_unInt32PropertyTag                                : uint   : 2; // unsigned int
k_unUint64PropertyTag                               : uint   : 3; // unsigned int
k_unBoolPropertyTag                                 : uint   : 4; // unsigned int
k_unStringPropertyTag                               : uint   : 5; // unsigned int
k_unHmdMatrix34PropertyTag                          : uint   : 20; // unsigned int
k_unHmdMatrix44PropertyTag                          : uint   : 21; // unsigned int
k_unHmdVector3PropertyTag                           : uint   : 22; // unsigned int
k_unHmdVector4PropertyTag                           : uint   : 23; // unsigned int
k_unHiddenAreaPropertyTag                           : uint   : 30; // unsigned int
k_unOpenVRInternalReserved_Start                    : uint   : 1000; // unsigned int
k_unOpenVRInternalReserved_End                      : uint   : 10000; // unsigned int
k_unMaxPropertyStringSize                           : uint   : 32768; // unsigned int
k_unControllerStateAxisCount                        : uint   : 5; // unsigned int
k_ulOverlayHandleInvalid                            : u64    : 0; // unsigned long
k_unScreenshotHandleInvalid                         : uint   : 0; // unsigned int
IVRSystem_Version                                   : string : "IVRSystem_015"; // char *
IVRExtendedDisplay_Version                          : string : "IVRExtendedDisplay_001"; // char *
IVRTrackedCamera_Version                            : string : "IVRTrackedCamera_003"; // char *
k_unMaxApplicationKeyLength                         : uint   : 128; // unsigned int
k_pch_MimeType_HomeApp                              : string : "vr/home"; // char *
k_pch_MimeType_GameTheater                          : string : "vr/game_theater"; // char *
IVRApplications_Version                             : string : "IVRApplications_006"; // char *
IVRChaperone_Version                                : string : "IVRChaperone_003"; // char *
IVRChaperoneSetup_Version                           : string : "IVRChaperoneSetup_005"; // char *
IVRCompositor_Version                               : string : "IVRCompositor_020"; // char *
k_unVROverlayMaxKeyLength                           : uint   : 128; // unsigned int
k_unVROverlayMaxNameLength                          : uint   : 128; // unsigned int
k_unMaxOverlayCount                                 : uint   : 64; // unsigned int
k_unMaxOverlayIntersectionMaskPrimitivesCount       : uint   : 32; // unsigned int
IVROverlay_Version                                  : string : "IVROverlay_014"; // char *
k_pch_Controller_Component_GDC2015                  : string : "gdc2015"; // char *
k_pch_Controller_Component_Base                     : string : "base"; // char *
k_pch_Controller_Component_Tip                      : string : "tip"; // char *
k_pch_Controller_Component_HandGrip                 : string : "handgrip"; // char *
k_pch_Controller_Component_Status                   : string : "status"; // char *
IVRRenderModels_Version                             : string : "IVRRenderModels_005"; // char *
k_unNotificationTextMaxSize                         : uint   : 256; // unsigned int
IVRNotifications_Version                            : string : "IVRNotifications_002"; // char *
k_unMaxSettingsKeyLength                            : uint   : 128; // unsigned int
IVRSettings_Version                                 : string : "IVRSettings_002"; // char *
k_pch_SteamVR_Section                               : string : "steamvr"; // char *
k_pch_SteamVR_RequireHmd_String                     : string : "requireHmd"; // char *
k_pch_SteamVR_ForcedDriverKey_String                : string : "forcedDriver"; // char *
k_pch_SteamVR_ForcedHmdKey_String                   : string : "forcedHmd"; // char *
k_pch_SteamVR_DisplayDebug_Bool                     : string : "displayDebug"; // char *
k_pch_SteamVR_DebugProcessPipe_String               : string : "debugProcessPipe"; // char *
k_pch_SteamVR_EnableDistortion_Bool                 : string : "enableDistortion"; // char *
k_pch_SteamVR_DisplayDebugX_Int32                   : string : "displayDebugX"; // char *
k_pch_SteamVR_DisplayDebugY_Int32                   : string : "displayDebugY"; // char *
k_pch_SteamVR_SendSystemButtonToAllApps_Bool        : string : "sendSystemButtonToAllApps"; // char *
k_pch_SteamVR_LogLevel_Int32                        : string : "loglevel"; // char *
k_pch_SteamVR_IPD_Float                             : string : "ipd"; // char *
k_pch_SteamVR_Background_String                     : string : "background"; // char *
k_pch_SteamVR_BackgroundUseDomeProjection_Bool      : string : "backgroundUseDomeProjection"; // char *
k_pch_SteamVR_BackgroundCameraHeight_Float          : string : "backgroundCameraHeight"; // char *
k_pch_SteamVR_BackgroundDomeRadius_Float            : string : "backgroundDomeRadius"; // char *
k_pch_SteamVR_GridColor_String                      : string : "gridColor"; // char *
k_pch_SteamVR_PlayAreaColor_String                  : string : "playAreaColor"; // char *
k_pch_SteamVR_ShowStage_Bool                        : string : "showStage"; // char *
k_pch_SteamVR_ActivateMultipleDrivers_Bool          : string : "activateMultipleDrivers"; // char *
k_pch_SteamVR_DirectMode_Bool                       : string : "directMode"; // char *
k_pch_SteamVR_DirectModeEdidVid_Int32               : string : "directModeEdidVid"; // char *
k_pch_SteamVR_DirectModeEdidPid_Int32               : string : "directModeEdidPid"; // char *
k_pch_SteamVR_UsingSpeakers_Bool                    : string : "usingSpeakers"; // char *
k_pch_SteamVR_SpeakersForwardYawOffsetDegrees_Float : string : "speakersForwardYawOffsetDegrees"; // char *
k_pch_SteamVR_BaseStationPowerManagement_Bool       : string : "basestationPowerManagement"; // char *
k_pch_SteamVR_NeverKillProcesses_Bool               : string : "neverKillProcesses"; // char *
k_pch_SteamVR_RenderTargetMultiplier_Float          : string : "renderTargetMultiplier"; // char *
k_pch_SteamVR_AllowAsyncReprojection_Bool           : string : "allowAsyncReprojection"; // char *
k_pch_SteamVR_AllowReprojection_Bool                : string : "allowInterleavedReprojection"; // char *
k_pch_SteamVR_ForceReprojection_Bool                : string : "forceReprojection"; // char *
k_pch_SteamVR_ForceFadeOnBadTracking_Bool           : string : "forceFadeOnBadTracking"; // char *
k_pch_SteamVR_DefaultMirrorView_Int32               : string : "defaultMirrorView"; // char *
k_pch_SteamVR_ShowMirrorView_Bool                   : string : "showMirrorView"; // char *
k_pch_SteamVR_MirrorViewGeometry_String             : string : "mirrorViewGeometry"; // char *
k_pch_SteamVR_StartMonitorFromAppLaunch             : string : "startMonitorFromAppLaunch"; // char *
k_pch_SteamVR_StartCompositorFromAppLaunch_Bool     : string : "startCompositorFromAppLaunch"; // char *
k_pch_SteamVR_StartDashboardFromAppLaunch_Bool      : string : "startDashboardFromAppLaunch"; // char *
k_pch_SteamVR_StartOverlayAppsFromDashboard_Bool    : string : "startOverlayAppsFromDashboard"; // char *
k_pch_SteamVR_EnableHomeApp                         : string : "enableHomeApp"; // char *
k_pch_SteamVR_SetInitialDefaultHomeApp              : string : "setInitialDefaultHomeApp"; // char *
k_pch_SteamVR_CycleBackgroundImageTimeSec_Int32     : string : "CycleBackgroundImageTimeSec"; // char *
k_pch_SteamVR_RetailDemo_Bool                       : string : "retailDemo"; // char *
k_pch_SteamVR_IpdOffset_Float                       : string : "ipdOffset"; // char *
k_pch_Lighthouse_Section                            : string : "driver_lighthouse"; // char *
k_pch_Lighthouse_DisableIMU_Bool                    : string : "disableimu"; // char *
k_pch_Lighthouse_UseDisambiguation_String           : string : "usedisambiguation"; // char *
k_pch_Lighthouse_DisambiguationDebug_Int32          : string : "disambiguationdebug"; // char *
k_pch_Lighthouse_PrimaryBasestation_Int32           : string : "primarybasestation"; // char *
k_pch_Lighthouse_DBHistory_Bool                     : string : "dbhistory"; // char *
k_pch_Null_Section                                  : string : "driver_null"; // char *
k_pch_Null_SerialNumber_String                      : string : "serialNumber"; // char *
k_pch_Null_ModelNumber_String                       : string : "modelNumber"; // char *
k_pch_Null_WindowX_Int32                            : string : "windowX"; // char *
k_pch_Null_WindowY_Int32                            : string : "windowY"; // char *
k_pch_Null_WindowWidth_Int32                        : string : "windowWidth"; // char *
k_pch_Null_WindowHeight_Int32                       : string : "windowHeight"; // char *
k_pch_Null_RenderWidth_Int32                        : string : "renderWidth"; // char *
k_pch_Null_RenderHeight_Int32                       : string : "renderHeight"; // char *
k_pch_Null_SecondsFromVsyncToPhotons_Float          : string : "secondsFromVsyncToPhotons"; // char *
k_pch_Null_DisplayFrequency_Float                   : string : "displayFrequency"; // char *
k_pch_UserInterface_Section                         : string : "userinterface"; // char *
k_pch_UserInterface_StatusAlwaysOnTop_Bool          : string : "StatusAlwaysOnTop"; // char *
k_pch_UserInterface_MinimizeToTray_Bool             : string : "MinimizeToTray"; // char *
k_pch_UserInterface_Screenshots_Bool                : string : "screenshots"; // char *
k_pch_UserInterface_ScreenshotType_Int              : string : "screenshotType"; // char *
k_pch_Notifications_Section                         : string : "notifications"; // char *
k_pch_Notifications_DoNotDisturb_Bool               : string : "DoNotDisturb"; // char *
k_pch_Keyboard_Section                              : string : "keyboard"; // char *
k_pch_Keyboard_TutorialCompletions                  : string : "TutorialCompletions"; // char *
k_pch_Keyboard_ScaleX                               : string : "ScaleX"; // char *
k_pch_Keyboard_ScaleY                               : string : "ScaleY"; // char *
k_pch_Keyboard_OffsetLeftX                          : string : "OffsetLeftX"; // char *
k_pch_Keyboard_OffsetRightX                         : string : "OffsetRightX"; // char *
k_pch_Keyboard_OffsetY                              : string : "OffsetY"; // char *
k_pch_Keyboard_Smoothing                            : string : "Smoothing"; // char *
k_pch_Perf_Section                                  : string : "perfcheck"; // char *
k_pch_Perf_HeuristicActive_Bool                     : string : "heuristicActive"; // char *
k_pch_Perf_NotifyInHMD_Bool                         : string : "warnInHMD"; // char *
k_pch_Perf_NotifyOnlyOnce_Bool                      : string : "warnOnlyOnce"; // char *
k_pch_Perf_AllowTimingStore_Bool                    : string : "allowTimingStore"; // char *
k_pch_Perf_SaveTimingsOnExit_Bool                   : string : "saveTimingsOnExit"; // char *
k_pch_Perf_TestData_Float                           : string : "perfTestData"; // char *
k_pch_CollisionBounds_Section                       : string : "collisionBounds"; // char *
k_pch_CollisionBounds_Style_Int32                   : string : "CollisionBoundsStyle"; // char *
k_pch_CollisionBounds_GroundPerimeterOn_Bool        : string : "CollisionBoundsGroundPerimeterOn"; // char *
k_pch_CollisionBounds_CenterMarkerOn_Bool           : string : "CollisionBoundsCenterMarkerOn"; // char *
k_pch_CollisionBounds_PlaySpaceOn_Bool              : string : "CollisionBoundsPlaySpaceOn"; // char *
k_pch_CollisionBounds_FadeDistance_Float            : string : "CollisionBoundsFadeDistance"; // char *
k_pch_CollisionBounds_ColorGammaR_Int32             : string : "CollisionBoundsColorGammaR"; // char *
k_pch_CollisionBounds_ColorGammaG_Int32             : string : "CollisionBoundsColorGammaG"; // char *
k_pch_CollisionBounds_ColorGammaB_Int32             : string : "CollisionBoundsColorGammaB"; // char *
k_pch_CollisionBounds_ColorGammaA_Int32             : string : "CollisionBoundsColorGammaA"; // char *
k_pch_Camera_Section                                : string : "camera"; // char *
k_pch_Camera_EnableCamera_Bool                      : string : "enableCamera"; // char *
k_pch_Camera_EnableCameraInDashboard_Bool           : string : "enableCameraInDashboard"; // char *
k_pch_Camera_EnableCameraForCollisionBounds_Bool    : string : "enableCameraForCollisionBounds"; // char *
k_pch_Camera_EnableCameraForRoomView_Bool           : string : "enableCameraForRoomView"; // char *
k_pch_Camera_BoundsColorGammaR_Int32                : string : "cameraBoundsColorGammaR"; // char *
k_pch_Camera_BoundsColorGammaG_Int32                : string : "cameraBoundsColorGammaG"; // char *
k_pch_Camera_BoundsColorGammaB_Int32                : string : "cameraBoundsColorGammaB"; // char *
k_pch_Camera_BoundsColorGammaA_Int32                : string : "cameraBoundsColorGammaA"; // char *
k_pch_Camera_BoundsStrength_Int32                   : string : "cameraBoundsStrength"; // char *
k_pch_audio_Section                                 : string : "audio"; // char *
k_pch_audio_OnPlaybackDevice_String                 : string : "onPlaybackDevice"; // char *
k_pch_audio_OnRecordDevice_String                   : string : "onRecordDevice"; // char *
k_pch_audio_OnPlaybackMirrorDevice_String           : string : "onPlaybackMirrorDevice"; // char *
k_pch_audio_OffPlaybackDevice_String                : string : "offPlaybackDevice"; // char *
k_pch_audio_OffRecordDevice_String                  : string : "offRecordDevice"; // char *
k_pch_audio_VIVEHDMIGain                            : string : "viveHDMIGain"; // char *
k_pch_Power_Section                                 : string : "power"; // char *
k_pch_Power_PowerOffOnExit_Bool                     : string : "powerOffOnExit"; // char *
k_pch_Power_TurnOffScreensTimeout_Float             : string : "turnOffScreensTimeout"; // char *
k_pch_Power_TurnOffControllersTimeout_Float         : string : "turnOffControllersTimeout"; // char *
k_pch_Power_ReturnToWatchdogTimeout_Float           : string : "returnToWatchdogTimeout"; // char *
k_pch_Power_AutoLaunchSteamVROnButtonPress          : string : "autoLaunchSteamVROnButtonPress"; // char *
k_pch_Dashboard_Section                             : string : "dashboard"; // char *
k_pch_Dashboard_EnableDashboard_Bool                : string : "enableDashboard"; // char *
k_pch_Dashboard_ArcadeMode_Bool                     : string : "arcadeMode"; // char *
k_pch_modelskin_Section                             : string : "modelskins"; // char *
k_pch_Driver_Enable_Bool                            : string : "enable"; // char *
IVRScreenshots_Version                              : string : "IVRScreenshots_001"; // char *
IVRResources_Version                                : string : "IVRResources_001"; // char *


// OpenVR Enums

EVREye :: enum {
	EVREye_Eye_Left = 0,
	EVREye_Eye_Right = 1,
}

ETextureType :: enum {
	ETextureType_TextureType_DirectX = 0,
	ETextureType_TextureType_OpenGL = 1,
	ETextureType_TextureType_Vulkan = 2,
	ETextureType_TextureType_IOSurface = 3,
	ETextureType_TextureType_DirectX12 = 4,
}

EColorSpace :: enum {
	EColorSpace_ColorSpace_Auto = 0,
	EColorSpace_ColorSpace_Gamma = 1,
	EColorSpace_ColorSpace_Linear = 2,
}

ETrackingResult :: enum {
	ETrackingResult_TrackingResult_Uninitialized = 1,
	ETrackingResult_TrackingResult_Calibrating_InProgress = 100,
	ETrackingResult_TrackingResult_Calibrating_OutOfRange = 101,
	ETrackingResult_TrackingResult_Running_OK = 200,
	ETrackingResult_TrackingResult_Running_OutOfRange = 201,
}

ETrackedDeviceClass :: enum {
	ETrackedDeviceClass_TrackedDeviceClass_Invalid = 0,
	ETrackedDeviceClass_TrackedDeviceClass_HMD = 1,
	ETrackedDeviceClass_TrackedDeviceClass_Controller = 2,
	ETrackedDeviceClass_TrackedDeviceClass_GenericTracker = 3,
	ETrackedDeviceClass_TrackedDeviceClass_TrackingReference = 4,
}

ETrackedControllerRole :: enum {
	ETrackedControllerRole_TrackedControllerRole_Invalid = 0,
	ETrackedControllerRole_TrackedControllerRole_LeftHand = 1,
	ETrackedControllerRole_TrackedControllerRole_RightHand = 2,
}

ETrackingUniverseOrigin :: enum {
	ETrackingUniverseOrigin_TrackingUniverseSeated = 0,
	ETrackingUniverseOrigin_TrackingUniverseStanding = 1,
	ETrackingUniverseOrigin_TrackingUniverseRawAndUncalibrated = 2,
}

ETrackedDeviceProperty :: enum {
	ETrackedDeviceProperty_Prop_Invalid = 0,
	ETrackedDeviceProperty_Prop_TrackingSystemName_String = 1000,
	ETrackedDeviceProperty_Prop_ModelNumber_String = 1001,
	ETrackedDeviceProperty_Prop_SerialNumber_String = 1002,
	ETrackedDeviceProperty_Prop_RenderModelName_String = 1003,
	ETrackedDeviceProperty_Prop_WillDriftInYaw_Bool = 1004,
	ETrackedDeviceProperty_Prop_ManufacturerName_String = 1005,
	ETrackedDeviceProperty_Prop_TrackingFirmwareVersion_String = 1006,
	ETrackedDeviceProperty_Prop_HardwareRevision_String = 1007,
	ETrackedDeviceProperty_Prop_AllWirelessDongleDescriptions_String = 1008,
	ETrackedDeviceProperty_Prop_ConnectedWirelessDongle_String = 1009,
	ETrackedDeviceProperty_Prop_DeviceIsWireless_Bool = 1010,
	ETrackedDeviceProperty_Prop_DeviceIsCharging_Bool = 1011,
	ETrackedDeviceProperty_Prop_DeviceBatteryPercentage_Float = 1012,
	ETrackedDeviceProperty_Prop_StatusDisplayTransform_Matrix34 = 1013,
	ETrackedDeviceProperty_Prop_Firmware_UpdateAvailable_Bool = 1014,
	ETrackedDeviceProperty_Prop_Firmware_ManualUpdate_Bool = 1015,
	ETrackedDeviceProperty_Prop_Firmware_ManualUpdateURL_String = 1016,
	ETrackedDeviceProperty_Prop_HardwareRevision_Uint64 = 1017,
	ETrackedDeviceProperty_Prop_FirmwareVersion_Uint64 = 1018,
	ETrackedDeviceProperty_Prop_FPGAVersion_Uint64 = 1019,
	ETrackedDeviceProperty_Prop_VRCVersion_Uint64 = 1020,
	ETrackedDeviceProperty_Prop_RadioVersion_Uint64 = 1021,
	ETrackedDeviceProperty_Prop_DongleVersion_Uint64 = 1022,
	ETrackedDeviceProperty_Prop_BlockServerShutdown_Bool = 1023,
	ETrackedDeviceProperty_Prop_CanUnifyCoordinateSystemWithHmd_Bool = 1024,
	ETrackedDeviceProperty_Prop_ContainsProximitySensor_Bool = 1025,
	ETrackedDeviceProperty_Prop_DeviceProvidesBatteryStatus_Bool = 1026,
	ETrackedDeviceProperty_Prop_DeviceCanPowerOff_Bool = 1027,
	ETrackedDeviceProperty_Prop_Firmware_ProgrammingTarget_String = 1028,
	ETrackedDeviceProperty_Prop_DeviceClass_Int32 = 1029,
	ETrackedDeviceProperty_Prop_HasCamera_Bool = 1030,
	ETrackedDeviceProperty_Prop_DriverVersion_String = 1031,
	ETrackedDeviceProperty_Prop_Firmware_ForceUpdateRequired_Bool = 1032,
	ETrackedDeviceProperty_Prop_ViveSystemButtonFixRequired_Bool = 1033,
	ETrackedDeviceProperty_Prop_ParentDriver_Uint64 = 1034,
	ETrackedDeviceProperty_Prop_ReportsTimeSinceVSync_Bool = 2000,
	ETrackedDeviceProperty_Prop_SecondsFromVsyncToPhotons_Float = 2001,
	ETrackedDeviceProperty_Prop_DisplayFrequency_Float = 2002,
	ETrackedDeviceProperty_Prop_UserIpdMeters_Float = 2003,
	ETrackedDeviceProperty_Prop_CurrentUniverseId_Uint64 = 2004,
	ETrackedDeviceProperty_Prop_PreviousUniverseId_Uint64 = 2005,
	ETrackedDeviceProperty_Prop_DisplayFirmwareVersion_Uint64 = 2006,
	ETrackedDeviceProperty_Prop_IsOnDesktop_Bool = 2007,
	ETrackedDeviceProperty_Prop_DisplayMCType_Int32 = 2008,
	ETrackedDeviceProperty_Prop_DisplayMCOffset_Float = 2009,
	ETrackedDeviceProperty_Prop_DisplayMCScale_Float = 2010,
	ETrackedDeviceProperty_Prop_EdidVendorID_Int32 = 2011,
	ETrackedDeviceProperty_Prop_DisplayMCImageLeft_String = 2012,
	ETrackedDeviceProperty_Prop_DisplayMCImageRight_String = 2013,
	ETrackedDeviceProperty_Prop_DisplayGCBlackClamp_Float = 2014,
	ETrackedDeviceProperty_Prop_EdidProductID_Int32 = 2015,
	ETrackedDeviceProperty_Prop_CameraToHeadTransform_Matrix34 = 2016,
	ETrackedDeviceProperty_Prop_DisplayGCType_Int32 = 2017,
	ETrackedDeviceProperty_Prop_DisplayGCOffset_Float = 2018,
	ETrackedDeviceProperty_Prop_DisplayGCScale_Float = 2019,
	ETrackedDeviceProperty_Prop_DisplayGCPrescale_Float = 2020,
	ETrackedDeviceProperty_Prop_DisplayGCImage_String = 2021,
	ETrackedDeviceProperty_Prop_LensCenterLeftU_Float = 2022,
	ETrackedDeviceProperty_Prop_LensCenterLeftV_Float = 2023,
	ETrackedDeviceProperty_Prop_LensCenterRightU_Float = 2024,
	ETrackedDeviceProperty_Prop_LensCenterRightV_Float = 2025,
	ETrackedDeviceProperty_Prop_UserHeadToEyeDepthMeters_Float = 2026,
	ETrackedDeviceProperty_Prop_CameraFirmwareVersion_Uint64 = 2027,
	ETrackedDeviceProperty_Prop_CameraFirmwareDescription_String = 2028,
	ETrackedDeviceProperty_Prop_DisplayFPGAVersion_Uint64 = 2029,
	ETrackedDeviceProperty_Prop_DisplayBootloaderVersion_Uint64 = 2030,
	ETrackedDeviceProperty_Prop_DisplayHardwareVersion_Uint64 = 2031,
	ETrackedDeviceProperty_Prop_AudioFirmwareVersion_Uint64 = 2032,
	ETrackedDeviceProperty_Prop_CameraCompatibilityMode_Int32 = 2033,
	ETrackedDeviceProperty_Prop_ScreenshotHorizontalFieldOfViewDegrees_Float = 2034,
	ETrackedDeviceProperty_Prop_ScreenshotVerticalFieldOfViewDegrees_Float = 2035,
	ETrackedDeviceProperty_Prop_DisplaySuppressed_Bool = 2036,
	ETrackedDeviceProperty_Prop_DisplayAllowNightMode_Bool = 2037,
	ETrackedDeviceProperty_Prop_DisplayMCImageWidth_Int32 = 2038,
	ETrackedDeviceProperty_Prop_DisplayMCImageHeight_Int32 = 2039,
	ETrackedDeviceProperty_Prop_DisplayMCImageNumChannels_Int32 = 2040,
	ETrackedDeviceProperty_Prop_DisplayMCImageData_Binary = 2041,
	ETrackedDeviceProperty_Prop_UsesDriverDirectMode_Bool = 2042,
	ETrackedDeviceProperty_Prop_AttachedDeviceId_String = 3000,
	ETrackedDeviceProperty_Prop_SupportedButtons_Uint64 = 3001,
	ETrackedDeviceProperty_Prop_Axis0Type_Int32 = 3002,
	ETrackedDeviceProperty_Prop_Axis1Type_Int32 = 3003,
	ETrackedDeviceProperty_Prop_Axis2Type_Int32 = 3004,
	ETrackedDeviceProperty_Prop_Axis3Type_Int32 = 3005,
	ETrackedDeviceProperty_Prop_Axis4Type_Int32 = 3006,
	ETrackedDeviceProperty_Prop_ControllerRoleHint_Int32 = 3007,
	ETrackedDeviceProperty_Prop_FieldOfViewLeftDegrees_Float = 4000,
	ETrackedDeviceProperty_Prop_FieldOfViewRightDegrees_Float = 4001,
	ETrackedDeviceProperty_Prop_FieldOfViewTopDegrees_Float = 4002,
	ETrackedDeviceProperty_Prop_FieldOfViewBottomDegrees_Float = 4003,
	ETrackedDeviceProperty_Prop_TrackingRangeMinimumMeters_Float = 4004,
	ETrackedDeviceProperty_Prop_TrackingRangeMaximumMeters_Float = 4005,
	ETrackedDeviceProperty_Prop_ModeLabel_String = 4006,
	ETrackedDeviceProperty_Prop_IconPathName_String = 5000,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceOff_String = 5001,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceSearching_String = 5002,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceSearchingAlert_String = 5003,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceReady_String = 5004,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceReadyAlert_String = 5005,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceNotReady_String = 5006,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceStandby_String = 5007,
	ETrackedDeviceProperty_Prop_NamedIconPathDeviceAlertLow_String = 5008,
	ETrackedDeviceProperty_Prop_DisplayHiddenArea_Binary_Start = 5100,
	ETrackedDeviceProperty_Prop_DisplayHiddenArea_Binary_End = 5150,
	ETrackedDeviceProperty_Prop_UserConfigPath_String = 6000,
	ETrackedDeviceProperty_Prop_InstallPath_String = 6001,
	ETrackedDeviceProperty_Prop_VendorSpecific_Reserved_Start = 10000,
	ETrackedDeviceProperty_Prop_VendorSpecific_Reserved_End = 10999,
}

ETrackedPropertyError :: enum {
	ETrackedPropertyError_TrackedProp_Success = 0,
	ETrackedPropertyError_TrackedProp_WrongDataType = 1,
	ETrackedPropertyError_TrackedProp_WrongDeviceClass = 2,
	ETrackedPropertyError_TrackedProp_BufferTooSmall = 3,
	ETrackedPropertyError_TrackedProp_UnknownProperty = 4,
	ETrackedPropertyError_TrackedProp_InvalidDevice = 5,
	ETrackedPropertyError_TrackedProp_CouldNotContactServer = 6,
	ETrackedPropertyError_TrackedProp_ValueNotProvidedByDevice = 7,
	ETrackedPropertyError_TrackedProp_StringExceedsMaximumLength = 8,
	ETrackedPropertyError_TrackedProp_NotYetAvailable = 9,
	ETrackedPropertyError_TrackedProp_PermissionDenied = 10,
	ETrackedPropertyError_TrackedProp_InvalidOperation = 11,
}

EVRSubmitFlags :: enum {
	EVRSubmitFlags_Submit_Default = 0,
	EVRSubmitFlags_Submit_LensDistortionAlreadyApplied = 1,
	EVRSubmitFlags_Submit_GlRenderBuffer = 2,
	EVRSubmitFlags_Submit_Reserved = 4,
}

EVRState :: enum {
	EVRState_VRState_Undefined = -1,
	EVRState_VRState_Off = 0,
	EVRState_VRState_Searching = 1,
	EVRState_VRState_Searching_Alert = 2,
	EVRState_VRState_Ready = 3,
	EVRState_VRState_Ready_Alert = 4,
	EVRState_VRState_NotReady = 5,
	EVRState_VRState_Standby = 6,
	EVRState_VRState_Ready_Alert_Low = 7,
}

EVREventType :: enum {
	EVREventType_VREvent_None = 0,
	EVREventType_VREvent_TrackedDeviceActivated = 100,
	EVREventType_VREvent_TrackedDeviceDeactivated = 101,
	EVREventType_VREvent_TrackedDeviceUpdated = 102,
	EVREventType_VREvent_TrackedDeviceUserInteractionStarted = 103,
	EVREventType_VREvent_TrackedDeviceUserInteractionEnded = 104,
	EVREventType_VREvent_IpdChanged = 105,
	EVREventType_VREvent_EnterStandbyMode = 106,
	EVREventType_VREvent_LeaveStandbyMode = 107,
	EVREventType_VREvent_TrackedDeviceRoleChanged = 108,
	EVREventType_VREvent_WatchdogWakeUpRequested = 109,
	EVREventType_VREvent_LensDistortionChanged = 110,
	EVREventType_VREvent_PropertyChanged = 111,
	EVREventType_VREvent_ButtonPress = 200,
	EVREventType_VREvent_ButtonUnpress = 201,
	EVREventType_VREvent_ButtonTouch = 202,
	EVREventType_VREvent_ButtonUntouch = 203,
	EVREventType_VREvent_MouseMove = 300,
	EVREventType_VREvent_MouseButtonDown = 301,
	EVREventType_VREvent_MouseButtonUp = 302,
	EVREventType_VREvent_FocusEnter = 303,
	EVREventType_VREvent_FocusLeave = 304,
	EVREventType_VREvent_Scroll = 305,
	EVREventType_VREvent_TouchPadMove = 306,
	EVREventType_VREvent_OverlayFocusChanged = 307,
	EVREventType_VREvent_InputFocusCaptured = 400,
	EVREventType_VREvent_InputFocusReleased = 401,
	EVREventType_VREvent_SceneFocusLost = 402,
	EVREventType_VREvent_SceneFocusGained = 403,
	EVREventType_VREvent_SceneApplicationChanged = 404,
	EVREventType_VREvent_SceneFocusChanged = 405,
	EVREventType_VREvent_InputFocusChanged = 406,
	EVREventType_VREvent_SceneApplicationSecondaryRenderingStarted = 407,
	EVREventType_VREvent_HideRenderModels = 410,
	EVREventType_VREvent_ShowRenderModels = 411,
	EVREventType_VREvent_OverlayShown = 500,
	EVREventType_VREvent_OverlayHidden = 501,
	EVREventType_VREvent_DashboardActivated = 502,
	EVREventType_VREvent_DashboardDeactivated = 503,
	EVREventType_VREvent_DashboardThumbSelected = 504,
	EVREventType_VREvent_DashboardRequested = 505,
	EVREventType_VREvent_ResetDashboard = 506,
	EVREventType_VREvent_RenderToast = 507,
	EVREventType_VREvent_ImageLoaded = 508,
	EVREventType_VREvent_ShowKeyboard = 509,
	EVREventType_VREvent_HideKeyboard = 510,
	EVREventType_VREvent_OverlayGamepadFocusGained = 511,
	EVREventType_VREvent_OverlayGamepadFocusLost = 512,
	EVREventType_VREvent_OverlaySharedTextureChanged = 513,
	EVREventType_VREvent_DashboardGuideButtonDown = 514,
	EVREventType_VREvent_DashboardGuideButtonUp = 515,
	EVREventType_VREvent_ScreenshotTriggered = 516,
	EVREventType_VREvent_ImageFailed = 517,
	EVREventType_VREvent_DashboardOverlayCreated = 518,
	EVREventType_VREvent_RequestScreenshot = 520,
	EVREventType_VREvent_ScreenshotTaken = 521,
	EVREventType_VREvent_ScreenshotFailed = 522,
	EVREventType_VREvent_SubmitScreenshotToDashboard = 523,
	EVREventType_VREvent_ScreenshotProgressToDashboard = 524,
	EVREventType_VREvent_PrimaryDashboardDeviceChanged = 525,
	EVREventType_VREvent_Notification_Shown = 600,
	EVREventType_VREvent_Notification_Hidden = 601,
	EVREventType_VREvent_Notification_BeginInteraction = 602,
	EVREventType_VREvent_Notification_Destroyed = 603,
	EVREventType_VREvent_Quit = 700,
	EVREventType_VREvent_ProcessQuit = 701,
	EVREventType_VREvent_QuitAborted_UserPrompt = 702,
	EVREventType_VREvent_QuitAcknowledged = 703,
	EVREventType_VREvent_DriverRequestedQuit = 704,
	EVREventType_VREvent_ChaperoneDataHasChanged = 800,
	EVREventType_VREvent_ChaperoneUniverseHasChanged = 801,
	EVREventType_VREvent_ChaperoneTempDataHasChanged = 802,
	EVREventType_VREvent_ChaperoneSettingsHaveChanged = 803,
	EVREventType_VREvent_SeatedZeroPoseReset = 804,
	EVREventType_VREvent_AudioSettingsHaveChanged = 820,
	EVREventType_VREvent_BackgroundSettingHasChanged = 850,
	EVREventType_VREvent_CameraSettingsHaveChanged = 851,
	EVREventType_VREvent_ReprojectionSettingHasChanged = 852,
	EVREventType_VREvent_ModelSkinSettingsHaveChanged = 853,
	EVREventType_VREvent_EnvironmentSettingsHaveChanged = 854,
	EVREventType_VREvent_PowerSettingsHaveChanged = 855,
	EVREventType_VREvent_StatusUpdate = 900,
	EVREventType_VREvent_MCImageUpdated = 1000,
	EVREventType_VREvent_FirmwareUpdateStarted = 1100,
	EVREventType_VREvent_FirmwareUpdateFinished = 1101,
	EVREventType_VREvent_KeyboardClosed = 1200,
	EVREventType_VREvent_KeyboardCharInput = 1201,
	EVREventType_VREvent_KeyboardDone = 1202,
	EVREventType_VREvent_ApplicationTransitionStarted = 1300,
	EVREventType_VREvent_ApplicationTransitionAborted = 1301,
	EVREventType_VREvent_ApplicationTransitionNewAppStarted = 1302,
	EVREventType_VREvent_ApplicationListUpdated = 1303,
	EVREventType_VREvent_ApplicationMimeTypeLoad = 1304,
	EVREventType_VREvent_ApplicationTransitionNewAppLaunchComplete = 1305,
	EVREventType_VREvent_Compositor_MirrorWindowShown = 1400,
	EVREventType_VREvent_Compositor_MirrorWindowHidden = 1401,
	EVREventType_VREvent_Compositor_ChaperoneBoundsShown = 1410,
	EVREventType_VREvent_Compositor_ChaperoneBoundsHidden = 1411,
	EVREventType_VREvent_TrackedCamera_StartVideoStream = 1500,
	EVREventType_VREvent_TrackedCamera_StopVideoStream = 1501,
	EVREventType_VREvent_TrackedCamera_PauseVideoStream = 1502,
	EVREventType_VREvent_TrackedCamera_ResumeVideoStream = 1503,
	EVREventType_VREvent_TrackedCamera_EditingSurface = 1550,
	EVREventType_VREvent_PerformanceTest_EnableCapture = 1600,
	EVREventType_VREvent_PerformanceTest_DisableCapture = 1601,
	EVREventType_VREvent_PerformanceTest_FidelityLevel = 1602,
	EVREventType_VREvent_MessageOverlay_Closed = 1650,
	EVREventType_VREvent_VendorSpecific_Reserved_Start = 10000,
	EVREventType_VREvent_VendorSpecific_Reserved_End = 19999,
}

EDeviceActivityLevel :: enum {
	EDeviceActivityLevel_k_EDeviceActivityLevel_Unknown = -1,
	EDeviceActivityLevel_k_EDeviceActivityLevel_Idle = 0,
	EDeviceActivityLevel_k_EDeviceActivityLevel_UserInteraction = 1,
	EDeviceActivityLevel_k_EDeviceActivityLevel_UserInteraction_Timeout = 2,
	EDeviceActivityLevel_k_EDeviceActivityLevel_Standby = 3,
}

EVRButtonId :: enum {
	EVRButtonId_k_EButton_System = 0,
	EVRButtonId_k_EButton_ApplicationMenu = 1,
	EVRButtonId_k_EButton_Grip = 2,
	EVRButtonId_k_EButton_DPad_Left = 3,
	EVRButtonId_k_EButton_DPad_Up = 4,
	EVRButtonId_k_EButton_DPad_Right = 5,
	EVRButtonId_k_EButton_DPad_Down = 6,
	EVRButtonId_k_EButton_A = 7,
	EVRButtonId_k_EButton_ProximitySensor = 31,
	EVRButtonId_k_EButton_Axis0 = 32,
	EVRButtonId_k_EButton_Axis1 = 33,
	EVRButtonId_k_EButton_Axis2 = 34,
	EVRButtonId_k_EButton_Axis3 = 35,
	EVRButtonId_k_EButton_Axis4 = 36,
	EVRButtonId_k_EButton_SteamVR_Touchpad = 32,
	EVRButtonId_k_EButton_SteamVR_Trigger = 33,
	EVRButtonId_k_EButton_Dashboard_Back = 2,
	EVRButtonId_k_EButton_Max = 64,
}

EVRMouseButton :: enum {
	EVRMouseButton_VRMouseButton_Left = 1,
	EVRMouseButton_VRMouseButton_Right = 2,
	EVRMouseButton_VRMouseButton_Middle = 4,
}

EHiddenAreaMeshType :: enum {
	EHiddenAreaMeshType_k_eHiddenAreaMesh_Standard = 0,
	EHiddenAreaMeshType_k_eHiddenAreaMesh_Inverse = 1,
	EHiddenAreaMeshType_k_eHiddenAreaMesh_LineLoop = 2,
	EHiddenAreaMeshType_k_eHiddenAreaMesh_Max = 3,
}

EVRControllerAxisType :: enum {
	EVRControllerAxisType_k_eControllerAxis_None = 0,
	EVRControllerAxisType_k_eControllerAxis_TrackPad = 1,
	EVRControllerAxisType_k_eControllerAxis_Joystick = 2,
	EVRControllerAxisType_k_eControllerAxis_Trigger = 3,
}

EVRControllerEventOutputType :: enum {
	EVRControllerEventOutputType_ControllerEventOutput_OSEvents = 0,
	EVRControllerEventOutputType_ControllerEventOutput_VREvents = 1,
}

ECollisionBoundsStyle :: enum {
	ECollisionBoundsStyle_COLLISION_BOUNDS_STYLE_BEGINNER = 0,
	ECollisionBoundsStyle_COLLISION_BOUNDS_STYLE_INTERMEDIATE = 1,
	ECollisionBoundsStyle_COLLISION_BOUNDS_STYLE_SQUARES = 2,
	ECollisionBoundsStyle_COLLISION_BOUNDS_STYLE_ADVANCED = 3,
	ECollisionBoundsStyle_COLLISION_BOUNDS_STYLE_NONE = 4,
	ECollisionBoundsStyle_COLLISION_BOUNDS_STYLE_COUNT = 5,
}

EVROverlayError :: enum {
	EVROverlayError_VROverlayError_None = 0,
	EVROverlayError_VROverlayError_UnknownOverlay = 10,
	EVROverlayError_VROverlayError_InvalidHandle = 11,
	EVROverlayError_VROverlayError_PermissionDenied = 12,
	EVROverlayError_VROverlayError_OverlayLimitExceeded = 13,
	EVROverlayError_VROverlayError_WrongVisibilityType = 14,
	EVROverlayError_VROverlayError_KeyTooLong = 15,
	EVROverlayError_VROverlayError_NameTooLong = 16,
	EVROverlayError_VROverlayError_KeyInUse = 17,
	EVROverlayError_VROverlayError_WrongTransformType = 18,
	EVROverlayError_VROverlayError_InvalidTrackedDevice = 19,
	EVROverlayError_VROverlayError_InvalidParameter = 20,
	EVROverlayError_VROverlayError_ThumbnailCantBeDestroyed = 21,
	EVROverlayError_VROverlayError_ArrayTooSmall = 22,
	EVROverlayError_VROverlayError_RequestFailed = 23,
	EVROverlayError_VROverlayError_InvalidTexture = 24,
	EVROverlayError_VROverlayError_UnableToLoadFile = 25,
	EVROverlayError_VROverlayError_KeyboardAlreadyInUse = 26,
	EVROverlayError_VROverlayError_NoNeighbor = 27,
	EVROverlayError_VROverlayError_TooManyMaskPrimitives = 29,
	EVROverlayError_VROverlayError_BadMaskPrimitive = 30,
}

EVRApplicationType :: enum {
	EVRApplicationType_VRApplication_Other = 0,
	EVRApplicationType_VRApplication_Scene = 1,
	EVRApplicationType_VRApplication_Overlay = 2,
	EVRApplicationType_VRApplication_Background = 3,
	EVRApplicationType_VRApplication_Utility = 4,
	EVRApplicationType_VRApplication_VRMonitor = 5,
	EVRApplicationType_VRApplication_SteamWatchdog = 6,
	EVRApplicationType_VRApplication_Max = 7,
}

EVRFirmwareError :: enum {
	EVRFirmwareError_VRFirmwareError_None = 0,
	EVRFirmwareError_VRFirmwareError_Success = 1,
	EVRFirmwareError_VRFirmwareError_Fail = 2,
}

EVRNotificationError :: enum {
	EVRNotificationError_VRNotificationError_OK = 0,
	EVRNotificationError_VRNotificationError_InvalidNotificationId = 100,
	EVRNotificationError_VRNotificationError_NotificationQueueFull = 101,
	EVRNotificationError_VRNotificationError_InvalidOverlayHandle = 102,
	EVRNotificationError_VRNotificationError_SystemWithUserValueAlreadyExists = 103,
}

EVRInitError :: enum {
	EVRInitError_VRInitError_None = 0,
	EVRInitError_VRInitError_Unknown = 1,
	EVRInitError_VRInitError_Init_InstallationNotFound = 100,
	EVRInitError_VRInitError_Init_InstallationCorrupt = 101,
	EVRInitError_VRInitError_Init_VRClientDLLNotFound = 102,
	EVRInitError_VRInitError_Init_FileNotFound = 103,
	EVRInitError_VRInitError_Init_FactoryNotFound = 104,
	EVRInitError_VRInitError_Init_InterfaceNotFound = 105,
	EVRInitError_VRInitError_Init_InvalidInterface = 106,
	EVRInitError_VRInitError_Init_UserConfigDirectoryInvalid = 107,
	EVRInitError_VRInitError_Init_HmdNotFound = 108,
	EVRInitError_VRInitError_Init_NotInitialized = 109,
	EVRInitError_VRInitError_Init_PathRegistryNotFound = 110,
	EVRInitError_VRInitError_Init_NoConfigPath = 111,
	EVRInitError_VRInitError_Init_NoLogPath = 112,
	EVRInitError_VRInitError_Init_PathRegistryNotWritable = 113,
	EVRInitError_VRInitError_Init_AppInfoInitFailed = 114,
	EVRInitError_VRInitError_Init_Retry = 115,
	EVRInitError_VRInitError_Init_InitCanceledByUser = 116,
	EVRInitError_VRInitError_Init_AnotherAppLaunching = 117,
	EVRInitError_VRInitError_Init_SettingsInitFailed = 118,
	EVRInitError_VRInitError_Init_ShuttingDown = 119,
	EVRInitError_VRInitError_Init_TooManyObjects = 120,
	EVRInitError_VRInitError_Init_NoServerForBackgroundApp = 121,
	EVRInitError_VRInitError_Init_NotSupportedWithCompositor = 122,
	EVRInitError_VRInitError_Init_NotAvailableToUtilityApps = 123,
	EVRInitError_VRInitError_Init_Internal = 124,
	EVRInitError_VRInitError_Init_HmdDriverIdIsNone = 125,
	EVRInitError_VRInitError_Init_HmdNotFoundPresenceFailed = 126,
	EVRInitError_VRInitError_Init_VRMonitorNotFound = 127,
	EVRInitError_VRInitError_Init_VRMonitorStartupFailed = 128,
	EVRInitError_VRInitError_Init_LowPowerWatchdogNotSupported = 129,
	EVRInitError_VRInitError_Init_InvalidApplicationType = 130,
	EVRInitError_VRInitError_Init_NotAvailableToWatchdogApps = 131,
	EVRInitError_VRInitError_Init_WatchdogDisabledInSettings = 132,
	EVRInitError_VRInitError_Init_VRDashboardNotFound = 133,
	EVRInitError_VRInitError_Init_VRDashboardStartupFailed = 134,
	EVRInitError_VRInitError_Driver_Failed = 200,
	EVRInitError_VRInitError_Driver_Unknown = 201,
	EVRInitError_VRInitError_Driver_HmdUnknown = 202,
	EVRInitError_VRInitError_Driver_NotLoaded = 203,
	EVRInitError_VRInitError_Driver_RuntimeOutOfDate = 204,
	EVRInitError_VRInitError_Driver_HmdInUse = 205,
	EVRInitError_VRInitError_Driver_NotCalibrated = 206,
	EVRInitError_VRInitError_Driver_CalibrationInvalid = 207,
	EVRInitError_VRInitError_Driver_HmdDisplayNotFound = 208,
	EVRInitError_VRInitError_Driver_TrackedDeviceInterfaceUnknown = 209,
	EVRInitError_VRInitError_Driver_HmdDriverIdOutOfBounds = 211,
	EVRInitError_VRInitError_Driver_HmdDisplayMirrored = 212,
	EVRInitError_VRInitError_IPC_ServerInitFailed = 300,
	EVRInitError_VRInitError_IPC_ConnectFailed = 301,
	EVRInitError_VRInitError_IPC_SharedStateInitFailed = 302,
	EVRInitError_VRInitError_IPC_CompositorInitFailed = 303,
	EVRInitError_VRInitError_IPC_MutexInitFailed = 304,
	EVRInitError_VRInitError_IPC_Failed = 305,
	EVRInitError_VRInitError_IPC_CompositorConnectFailed = 306,
	EVRInitError_VRInitError_IPC_CompositorInvalidConnectResponse = 307,
	EVRInitError_VRInitError_IPC_ConnectFailedAfterMultipleAttempts = 308,
	EVRInitError_VRInitError_Compositor_Failed = 400,
	EVRInitError_VRInitError_Compositor_D3D11HardwareRequired = 401,
	EVRInitError_VRInitError_Compositor_FirmwareRequiresUpdate = 402,
	EVRInitError_VRInitError_Compositor_OverlayInitFailed = 403,
	EVRInitError_VRInitError_Compositor_ScreenshotsInitFailed = 404,
	EVRInitError_VRInitError_VendorSpecific_UnableToConnectToOculusRuntime = 1000,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_CantOpenDevice = 1101,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UnableToRequestConfigStart = 1102,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_NoStoredConfig = 1103,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_ConfigTooBig = 1104,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_ConfigTooSmall = 1105,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UnableToInitZLib = 1106,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_CantReadFirmwareVersion = 1107,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UnableToSendUserDataStart = 1108,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UnableToGetUserDataStart = 1109,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UnableToGetUserDataNext = 1110,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UserDataAddressRange = 1111,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_UserDataError = 1112,
	EVRInitError_VRInitError_VendorSpecific_HmdFound_ConfigFailedSanityCheck = 1113,
	EVRInitError_VRInitError_Steam_SteamInstallationNotFound = 2000,
}

EVRScreenshotType :: enum {
	EVRScreenshotType_VRScreenshotType_None = 0,
	EVRScreenshotType_VRScreenshotType_Mono = 1,
	EVRScreenshotType_VRScreenshotType_Stereo = 2,
	EVRScreenshotType_VRScreenshotType_Cubemap = 3,
	EVRScreenshotType_VRScreenshotType_MonoPanorama = 4,
	EVRScreenshotType_VRScreenshotType_StereoPanorama = 5,
}

EVRScreenshotPropertyFilenames :: enum {
	EVRScreenshotPropertyFilenames_VRScreenshotPropertyFilenames_Preview = 0,
	EVRScreenshotPropertyFilenames_VRScreenshotPropertyFilenames_VR = 1,
}

EVRTrackedCameraError :: enum {
	EVRTrackedCameraError_VRTrackedCameraError_None = 0,
	EVRTrackedCameraError_VRTrackedCameraError_OperationFailed = 100,
	EVRTrackedCameraError_VRTrackedCameraError_InvalidHandle = 101,
	EVRTrackedCameraError_VRTrackedCameraError_InvalidFrameHeaderVersion = 102,
	EVRTrackedCameraError_VRTrackedCameraError_OutOfHandles = 103,
	EVRTrackedCameraError_VRTrackedCameraError_IPCFailure = 104,
	EVRTrackedCameraError_VRTrackedCameraError_NotSupportedForThisDevice = 105,
	EVRTrackedCameraError_VRTrackedCameraError_SharedMemoryFailure = 106,
	EVRTrackedCameraError_VRTrackedCameraError_FrameBufferingFailure = 107,
	EVRTrackedCameraError_VRTrackedCameraError_StreamSetupFailure = 108,
	EVRTrackedCameraError_VRTrackedCameraError_InvalidGLTextureId = 109,
	EVRTrackedCameraError_VRTrackedCameraError_InvalidSharedTextureHandle = 110,
	EVRTrackedCameraError_VRTrackedCameraError_FailedToGetGLTextureId = 111,
	EVRTrackedCameraError_VRTrackedCameraError_SharedTextureFailure = 112,
	EVRTrackedCameraError_VRTrackedCameraError_NoFrameAvailable = 113,
	EVRTrackedCameraError_VRTrackedCameraError_InvalidArgument = 114,
	EVRTrackedCameraError_VRTrackedCameraError_InvalidFrameBufferSize = 115,
}

EVRTrackedCameraFrameType :: enum {
	EVRTrackedCameraFrameType_VRTrackedCameraFrameType_Distorted = 0,
	EVRTrackedCameraFrameType_VRTrackedCameraFrameType_Undistorted = 1,
	EVRTrackedCameraFrameType_VRTrackedCameraFrameType_MaximumUndistorted = 2,
	EVRTrackedCameraFrameType_MAX_CAMERA_FRAME_TYPES = 3,
}

EVRApplicationError :: enum {
	EVRApplicationError_VRApplicationError_None = 0,
	EVRApplicationError_VRApplicationError_AppKeyAlreadyExists = 100,
	EVRApplicationError_VRApplicationError_NoManifest = 101,
	EVRApplicationError_VRApplicationError_NoApplication = 102,
	EVRApplicationError_VRApplicationError_InvalidIndex = 103,
	EVRApplicationError_VRApplicationError_UnknownApplication = 104,
	EVRApplicationError_VRApplicationError_IPCFailed = 105,
	EVRApplicationError_VRApplicationError_ApplicationAlreadyRunning = 106,
	EVRApplicationError_VRApplicationError_InvalidManifest = 107,
	EVRApplicationError_VRApplicationError_InvalidApplication = 108,
	EVRApplicationError_VRApplicationError_LaunchFailed = 109,
	EVRApplicationError_VRApplicationError_ApplicationAlreadyStarting = 110,
	EVRApplicationError_VRApplicationError_LaunchInProgress = 111,
	EVRApplicationError_VRApplicationError_OldApplicationQuitting = 112,
	EVRApplicationError_VRApplicationError_TransitionAborted = 113,
	EVRApplicationError_VRApplicationError_IsTemplate = 114,
	EVRApplicationError_VRApplicationError_BufferTooSmall = 200,
	EVRApplicationError_VRApplicationError_PropertyNotSet = 201,
	EVRApplicationError_VRApplicationError_UnknownProperty = 202,
	EVRApplicationError_VRApplicationError_InvalidParameter = 203,
}

EVRApplicationProperty :: enum {
	EVRApplicationProperty_VRApplicationProperty_Name_String = 0,
	EVRApplicationProperty_VRApplicationProperty_LaunchType_String = 11,
	EVRApplicationProperty_VRApplicationProperty_WorkingDirectory_String = 12,
	EVRApplicationProperty_VRApplicationProperty_BinaryPath_String = 13,
	EVRApplicationProperty_VRApplicationProperty_Arguments_String = 14,
	EVRApplicationProperty_VRApplicationProperty_URL_String = 15,
	EVRApplicationProperty_VRApplicationProperty_Description_String = 50,
	EVRApplicationProperty_VRApplicationProperty_NewsURL_String = 51,
	EVRApplicationProperty_VRApplicationProperty_ImagePath_String = 52,
	EVRApplicationProperty_VRApplicationProperty_Source_String = 53,
	EVRApplicationProperty_VRApplicationProperty_IsDashboardOverlay_Bool = 60,
	EVRApplicationProperty_VRApplicationProperty_IsTemplate_Bool = 61,
	EVRApplicationProperty_VRApplicationProperty_IsInstanced_Bool = 62,
	EVRApplicationProperty_VRApplicationProperty_IsInternal_Bool = 63,
	EVRApplicationProperty_VRApplicationProperty_LastLaunchTime_Uint64 = 70,
}

EVRApplicationTransitionState :: enum {
	EVRApplicationTransitionState_VRApplicationTransition_None = 0,
	EVRApplicationTransitionState_VRApplicationTransition_OldAppQuitSent = 10,
	EVRApplicationTransitionState_VRApplicationTransition_WaitingForExternalLaunch = 11,
	EVRApplicationTransitionState_VRApplicationTransition_NewAppLaunched = 20,
}

ChaperoneCalibrationState :: enum {
	ChaperoneCalibrationState_OK = 1,
	ChaperoneCalibrationState_Warning = 100,
	ChaperoneCalibrationState_Warning_BaseStationMayHaveMoved = 101,
	ChaperoneCalibrationState_Warning_BaseStationRemoved = 102,
	ChaperoneCalibrationState_Warning_SeatedBoundsInvalid = 103,
	ChaperoneCalibrationState_Error = 200,
	ChaperoneCalibrationState_Error_BaseStationUninitialized = 201,
	ChaperoneCalibrationState_Error_BaseStationConflict = 202,
	ChaperoneCalibrationState_Error_PlayAreaInvalid = 203,
	ChaperoneCalibrationState_Error_CollisionBoundsInvalid = 204,
}

EChaperoneConfigFile :: enum {
	EChaperoneConfigFile_Live = 1,
	EChaperoneConfigFile_Temp = 2,
}

EChaperoneImportFlags :: enum {
	EChaperoneImportFlags_EChaperoneImport_BoundsOnly = 1,
}

EVRCompositorError :: enum {
	EVRCompositorError_VRCompositorError_None = 0,
	EVRCompositorError_VRCompositorError_RequestFailed = 1,
	EVRCompositorError_VRCompositorError_IncompatibleVersion = 100,
	EVRCompositorError_VRCompositorError_DoNotHaveFocus = 101,
	EVRCompositorError_VRCompositorError_InvalidTexture = 102,
	EVRCompositorError_VRCompositorError_IsNotSceneApplication = 103,
	EVRCompositorError_VRCompositorError_TextureIsOnWrongDevice = 104,
	EVRCompositorError_VRCompositorError_TextureUsesUnsupportedFormat = 105,
	EVRCompositorError_VRCompositorError_SharedTexturesNotSupported = 106,
	EVRCompositorError_VRCompositorError_IndexOutOfRange = 107,
	EVRCompositorError_VRCompositorError_AlreadySubmitted = 108,
}

VROverlayInputMethod :: enum {
	VROverlayInputMethod_None = 0,
	VROverlayInputMethod_Mouse = 1,
}

VROverlayTransformType :: enum {
	VROverlayTransformType_VROverlayTransform_Absolute = 0,
	VROverlayTransformType_VROverlayTransform_TrackedDeviceRelative = 1,
	VROverlayTransformType_VROverlayTransform_SystemOverlay = 2,
	VROverlayTransformType_VROverlayTransform_TrackedComponent = 3,
}

VROverlayFlags :: enum {
	VROverlayFlags_None = 0,
	VROverlayFlags_Curved = 1,
	VROverlayFlags_RGSS4X = 2,
	VROverlayFlags_NoDashboardTab = 3,
	VROverlayFlags_AcceptsGamepadEvents = 4,
	VROverlayFlags_ShowGamepadFocus = 5,
	VROverlayFlags_SendVRScrollEvents = 6,
	VROverlayFlags_SendVRTouchpadEvents = 7,
	VROverlayFlags_ShowTouchPadScrollWheel = 8,
	VROverlayFlags_TransferOwnershipToInternalProcess = 9,
	VROverlayFlags_SideBySide_Parallel = 10,
	VROverlayFlags_SideBySide_Crossed = 11,
	VROverlayFlags_Panorama = 12,
	VROverlayFlags_StereoPanorama = 13,
	VROverlayFlags_SortWithNonSceneOverlays = 14,
	VROverlayFlags_VisibleInDashboard = 15,
}

VRMessageOverlayResponse :: enum {
	VRMessageOverlayResponse_ButtonPress_0 = 0,
	VRMessageOverlayResponse_ButtonPress_1 = 1,
	VRMessageOverlayResponse_ButtonPress_2 = 2,
	VRMessageOverlayResponse_ButtonPress_3 = 3,
	VRMessageOverlayResponse_CouldntFindSystemOverlay = 4,
	VRMessageOverlayResponse_CouldntFindOrCreateClientOverlay = 5,
	VRMessageOverlayResponse_ApplicationQuit = 6,
}

EGamepadTextInputMode :: enum {
	EGamepadTextInputMode_k_EGamepadTextInputModeNormal = 0,
	EGamepadTextInputMode_k_EGamepadTextInputModePassword = 1,
	EGamepadTextInputMode_k_EGamepadTextInputModeSubmit = 2,
}

EGamepadTextInputLineMode :: enum {
	EGamepadTextInputLineMode_k_EGamepadTextInputLineModeSingleLine = 0,
	EGamepadTextInputLineMode_k_EGamepadTextInputLineModeMultipleLines = 1,
}

EOverlayDirection :: enum {
	EOverlayDirection_OverlayDirection_Up = 0,
	EOverlayDirection_OverlayDirection_Down = 1,
	EOverlayDirection_OverlayDirection_Left = 2,
	EOverlayDirection_OverlayDirection_Right = 3,
	EOverlayDirection_OverlayDirection_Count = 4,
}

EVROverlayIntersectionMaskPrimitiveType :: enum {
	EVROverlayIntersectionMaskPrimitiveType_OverlayIntersectionPrimitiveType_Rectangle = 0,
	EVROverlayIntersectionMaskPrimitiveType_OverlayIntersectionPrimitiveType_Circle = 1,
}

EVRRenderModelError :: enum {
	EVRRenderModelError_VRRenderModelError_None = 0,
	EVRRenderModelError_VRRenderModelError_Loading = 100,
	EVRRenderModelError_VRRenderModelError_NotSupported = 200,
	EVRRenderModelError_VRRenderModelError_InvalidArg = 300,
	EVRRenderModelError_VRRenderModelError_InvalidModel = 301,
	EVRRenderModelError_VRRenderModelError_NoShapes = 302,
	EVRRenderModelError_VRRenderModelError_MultipleShapes = 303,
	EVRRenderModelError_VRRenderModelError_TooManyVertices = 304,
	EVRRenderModelError_VRRenderModelError_MultipleTextures = 305,
	EVRRenderModelError_VRRenderModelError_BufferTooSmall = 306,
	EVRRenderModelError_VRRenderModelError_NotEnoughNormals = 307,
	EVRRenderModelError_VRRenderModelError_NotEnoughTexCoords = 308,
	EVRRenderModelError_VRRenderModelError_InvalidTexture = 400,
}

EVRComponentProperty :: enum {
	EVRComponentProperty_VRComponentProperty_IsStatic = 1,
	EVRComponentProperty_VRComponentProperty_IsVisible = 2,
	EVRComponentProperty_VRComponentProperty_IsTouched = 4,
	EVRComponentProperty_VRComponentProperty_IsPressed = 8,
	EVRComponentProperty_VRComponentProperty_IsScrolled = 16,
}

EVRNotificationType :: enum {
	EVRNotificationType_Transient = 0,
	EVRNotificationType_Persistent = 1,
	EVRNotificationType_Transient_SystemWithUserValue = 2,
}

EVRNotificationStyle :: enum {
	EVRNotificationStyle_None = 0,
	EVRNotificationStyle_Application = 100,
	EVRNotificationStyle_Contact_Disabled = 200,
	EVRNotificationStyle_Contact_Enabled = 201,
	EVRNotificationStyle_Contact_Active = 202,
}

EVRSettingsError :: enum {
	EVRSettingsError_VRSettingsError_None = 0,
	EVRSettingsError_VRSettingsError_IPCFailed = 1,
	EVRSettingsError_VRSettingsError_WriteFailed = 2,
	EVRSettingsError_VRSettingsError_ReadFailed = 3,
	EVRSettingsError_VRSettingsError_JsonParseFailed = 4,
	EVRSettingsError_VRSettingsError_UnsetSettingHasNoDefault = 5,
}

EVRScreenshotError :: enum {
	EVRScreenshotError_VRScreenshotError_None = 0,
	EVRScreenshotError_VRScreenshotError_RequestFailed = 1,
	EVRScreenshotError_VRScreenshotError_IncompatibleVersion = 100,
	EVRScreenshotError_VRScreenshotError_NotFound = 101,
	EVRScreenshotError_VRScreenshotError_BufferTooSmall = 102,
	EVRScreenshotError_VRScreenshotError_ScreenshotAlreadyInProgress = 108,
}


// OpenVR typedefs

TrackedDeviceIndex_t      :: u32;
VRNotificationId          :: u32;
VROverlayHandle_t         :: u64;

glSharedTextureHandle_t   :: rawptr;
glInt_t                   :: i32;
glUInt_t                  :: u32;
SharedTextureHandle_t     :: u64;
TrackedDeviceIndex_t      :: u32;
PropertyContainerHandle_t :: u64;
PropertyTypeTag_t         :: u32;
VROverlayHandle_t         :: u64;
TrackedCameraHandle_t     :: u64;
ScreenshotHandle_t        :: u32;
VRComponentProperties     :: u32;
TextureID_t               :: i32;
VRNotificationId          :: u32;
HmdError                  :: EVRInitError;
Hmd_Eye                   :: EVREye;
ColorSpace                :: EColorSpace;
HmdTrackingResult         :: ETrackingResult;
TrackedDeviceClass        :: ETrackedDeviceClass;
TrackingUniverseOrigin    :: ETrackingUniverseOrigin;
TrackedDeviceProperty     :: ETrackedDeviceProperty;
TrackedPropertyError      :: ETrackedPropertyError;
VRSubmitFlags_t           :: EVRSubmitFlags;
VRState_t                 :: EVRState;
CollisionBoundsStyle_t    :: ECollisionBoundsStyle;
VROverlayError            :: EVROverlayError;
VRFirmwareError           :: EVRFirmwareError;
VRCompositorError         :: EVRCompositorError;
VRScreenshotsError        :: EVRScreenshotError;

