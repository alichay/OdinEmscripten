#import win32 "sys/windows.odin";
#import "sys/wgl.odin";
#import "strings.odin";
#import "../../utils.odin";
#import "fmt.odin";

gl_library: win32.Hmodule;

sys_init :: proc() {
	lib_c_str := strings.new_c_string("opengl32.dll");
	gl_library = win32.LoadLibraryA(lib_c_str);
	free(lib_c_str);
}

map_gl_function :: proc(proc_ptr: rawptr, name: string) #inline {

	name_c_str := strings.new_c_string(name);
	defer(free(name_c_str));
	ptr := wgl.GetProcAddress(name_c_str);
	if ptr == nil {
		ptr = win32.GetProcAddress(gl_library, name_c_str);
	}
	(cast(^(proc() #cc_c))proc_ptr)^ = ptr;
}
