#load "gl_win32.odin" when ODIN_OS == "windows";
#load "gl_linux.odin" when ODIN_OS == "linux";

init :: proc() {

	// On win32, it's not as simple as loading procs from the library (for some stupid reason)
	// We have to grab wglGetProcAddress first, then use that to grab the other addresses.



	sys_init();

	map_gl_function(^gen_buffers,                 "glGenBuffers");
	map_gl_function(^gen_vertex_arrays,           "glGenVertexArrays");
	map_gl_function(^gen_samplers,                "glGenSamplers");
	map_gl_function(^bind_buffer,                 "glBindBuffer");
	map_gl_function(^bind_vertex_array,           "glBindVertexArray");
	map_gl_function(^bind_sampler,                "glBindSampler");
	map_gl_function(^buffer_data,                 "glBufferData");
	map_gl_function(^buffer_sub_data,             "glBufferSubData");
	map_gl_function(^draw_arrays,                 "glDrawArrays");
	map_gl_function(^draw_elements,               "glDrawElements");
	map_gl_function(^map_buffer,                  "glMapBuffer");
	map_gl_function(^unmap_buffer,                "glUnmapBuffer");
	map_gl_function(^vertex_attrib_pointer,       "glVertexAttribPointer");
	map_gl_function(^enable_vertex_attrib_array,  "glEnableVertexAttribArray");
	map_gl_function(^disable_vertex_attrib_array, "glDisableVertexAttribArray");
	map_gl_function(^create_shader,               "glCreateShader");
	map_gl_function(^shader_source,               "glShaderSource");
	map_gl_function(^compile_shader,              "glCompileShader");
	map_gl_function(^create_program,              "glCreateProgram");
	map_gl_function(^attach_shader,               "glAttachShader");
	map_gl_function(^detach_shader,               "glDetachShader");
	map_gl_function(^delete_shader,               "glDeleteShader");
	map_gl_function(^link_program,                "glLinkProgram");
	map_gl_function(^use_program,                 "glUseProgram");
	map_gl_function(^delete_program,              "glDeleteProgram");
	map_gl_function(^get_shaderiv,                "glGetShaderiv");
	map_gl_function(^get_programiv,               "glGetProgramiv");
	map_gl_function(^get_shader_info_log,         "glGetShaderInfoLog");
	map_gl_function(^get_program_info_log,        "glGetProgramInfoLog");
	map_gl_function(^active_texture,              "glActiveTexture");
	map_gl_function(^generate_mipmap,             "glGenerateMipmap");
	map_gl_function(^sampler_parameteri,          "glSamplerParameteri");
	map_gl_function(^sampler_parameterf,          "glSamplerParameterf");
	map_gl_function(^sampler_parameteriv,         "glSamplerParameteriv");
	map_gl_function(^sampler_parameterfv,         "glSamplerParameterfv");
	map_gl_function(^sampler_parameter_iiv,       "glSamplerParameterIiv");
	map_gl_function(^sampler_parameter_iuiv,      "glSamplerParameterIuiv");
	map_gl_function(^uniform1i,                   "glUniform1i");
	map_gl_function(^uniform2i,                   "glUniform2i");
	map_gl_function(^uniform3i,                   "glUniform3i");
	map_gl_function(^uniform4i,                   "glUniform4i");
	map_gl_function(^uniform1f,                   "glUniform1f");
	map_gl_function(^uniform2f,                   "glUniform2f");
	map_gl_function(^uniform3f,                   "glUniform3f");
	map_gl_function(^uniform4f,                   "glUniform4f");
	map_gl_function(^uniform_matrix4fv,           "glUniformMatrix4fv");
	map_gl_function(^get_uniform_location,        "glGetUniformLocation");
}

gen_buffers:       proc(count: i32, buffers: ^u32) #cc_c;
gen_vertex_arrays: proc(count: i32, buffers: ^u32) #cc_c;
gen_samplers:      proc(count: i32, buffers: ^u32) #cc_c;
bind_buffer:       proc(target: i32, buffer: u32) #cc_c;
bind_vertex_array: proc(buffer: u32) #cc_c;
bind_sampler:      proc(position: i32, sampler: u32) #cc_c;
buffer_data:       proc(target: i32, size: int, data: rawptr, usage: i32) #cc_c;
buffer_sub_data:   proc(target: i32, offset, size: int, data: rawptr) #cc_c;

draw_arrays:       proc(mode, first: i32, count: u32) #cc_c;
draw_elements:     proc(mode: i32, count: u32, type_: i32, indices: rawptr) #cc_c;

map_buffer:        proc(target, access: i32) -> rawptr #cc_c;
unmap_buffer:      proc(target: i32) #cc_c;

vertex_attrib_pointer:       proc(index: u32, size, type_: i32, normalized: i32, stride: u32, pointer: rawptr) #cc_c;
enable_vertex_attrib_array:  proc(index: u32) #cc_c;
disable_vertex_attrib_array: proc(index: u32) #cc_c;

create_shader:   proc(shader_type: i32) -> u32 #cc_c;
shader_source:   proc(shader: u32, count: u32, str: ^^byte, length: ^i32) #cc_c;
compile_shader:  proc(shader: u32) #cc_c;
create_program:  proc() -> u32 #cc_c;
attach_shader:   proc(program, shader: u32) #cc_c;
detach_shader:   proc(program, shader: u32) #cc_c;
delete_shader:   proc(shader:  u32) #cc_c;
link_program:    proc(program: u32) #cc_c;
use_program:     proc(program: u32) #cc_c;
delete_program:  proc(program: u32) #cc_c;


get_shaderiv:         proc(shader:  u32, pname: i32, params: ^i32) #cc_c;
get_programiv:        proc(program: u32, pname: i32, params: ^i32) #cc_c;
get_shader_info_log:  proc(shader:  u32, max_length: u32, length: ^u32, info_long: ^byte) #cc_c;
get_program_info_log: proc(program: u32, max_length: u32, length: ^u32, info_long: ^byte) #cc_c;

active_texture:   proc(texture: i32) #cc_c;
generate_mipmap:  proc(target:  i32) #cc_c;

sampler_parameteri:     proc(sampler: u32, pname: i32, param: i32) #cc_c;
sampler_parameterf:     proc(sampler: u32, pname: i32, param: f32) #cc_c;
sampler_parameteriv:    proc(sampler: u32, pname: i32, params: ^i32) #cc_c;
sampler_parameterfv:    proc(sampler: u32, pname: i32, params: ^f32) #cc_c;
sampler_parameter_iiv:  proc(sampler: u32, pname: i32, params: ^i32) #cc_c;
sampler_parameter_iuiv: proc(sampler: u32, pname: i32, params: ^u32) #cc_c;


uniform1i:         proc(loc: i32, v0: i32) #cc_c;
uniform2i:         proc(loc: i32, v0, v1: i32) #cc_c;
uniform3i:         proc(loc: i32, v0, v1, v2: i32) #cc_c;
uniform4i:         proc(loc: i32, v0, v1, v2, v3: i32) #cc_c;
uniform1f:         proc(loc: i32, v0: f32) #cc_c;
uniform2f:         proc(loc: i32, v0, v1: f32) #cc_c;
uniform3f:         proc(loc: i32, v0, v1, v2: f32) #cc_c;
uniform4f:         proc(loc: i32, v0, v1, v2, v3: f32) #cc_c;
uniform_matrix4fv: proc(loc: i32, count: u32, transpose: i32, value: ^f32) #cc_c;

get_uniform_location:  proc(program: u32, name: ^byte) -> i32 #cc_c;
