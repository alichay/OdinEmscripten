
#import "os.odin";
#import "fmt.odin";

gl_library: rawptr;

sys_init :: proc() {

	gl_library = os.dlopen("libGL.so", os.RTLD_NOW | os.RTLD_GLOBAL);
	if(gl_library == nil) {
		fmt.println(os.dlerror());
		os.exit(1);
	}

}

map_gl_function :: proc(proc_ptr: rawptr, name: string) #inline {
	(cast(^(proc() #cc_c))proc_ptr)^ = os.dlsym(gl_library, name);
}
