
#foreign_system_library ogl "-fOpenGL";

init :: proc() #inline {}

gen_buffers::       proc(count: i32, buffers: ^u32)                                                #foreign ogl "glGenBuffers";
gen_vertex_arrays:: proc(count: i32, buffers: ^u32)                                                #foreign ogl "glGenVertexArrays";
gen_samplers::      proc(count: i32, buffers: ^u32)                                                #foreign ogl "glGenSamplers";
bind_buffer::       proc(target: i32, buffer: u32)                                                 #foreign ogl "glBindBuffer";
bind_vertex_array:: proc(buffer: u32)                                                              #foreign ogl "glBindVertexArray";
bind_sampler::      proc(position: i32, sampler: u32)                                              #foreign ogl "glBindSampler";
buffer_data::       proc(target: i32, size: int, data: rawptr, usage: i32)                         #foreign ogl "glBufferData";
buffer_sub_data::   proc(target: i32, offset, size: int, data: rawptr)                             #foreign ogl "glBufferSubData";

draw_arrays::       proc(mode, first: i32, count: u32)                                             #foreign ogl "glDrawArrays";
draw_elements::     proc(mode: i32, count: u32, type_: i32, indices: rawptr)                       #foreign ogl "glDrawElements";

map_buffer::        proc(target, access: i32) -> rawptr                                            #foreign ogl "glMapBuffer";
unmap_buffer::      proc(target: i32)                                                              #foreign ogl "glUnmapBuffer";

vertex_attrib_pointer::       proc(index: u32, size, type_: i32, normalized: i32, stride: u32, pointer: rawptr) #foreign ogl "glVertexAttribPointer";
enable_vertex_attrib_array::  proc(index: u32)                                                     #foreign ogl "glEnableVertexAttribArray";
disable_vertex_attrib_array:: proc(index: u32)                                                     #foreign ogl "glDisableVertexAttribArray";

create_shader::   proc(shader_type: i32) -> u32                                                    #foreign ogl "glCreateShader";
shader_source::   proc(shader: u32, count: u32, str: ^^byte, length: ^i32)                         #foreign ogl "glShaderSource";
compile_shader::  proc(shader: u32)                                                                #foreign ogl "glCompileShader";
create_program::  proc() -> u32                                                                    #foreign ogl "glCreateProgram";
attach_shader::   proc(program, shader: u32)                                                       #foreign ogl "glAttachShader";
detach_shader::   proc(program, shader: u32)                                                       #foreign ogl "glDetachShader";
delete_shader::   proc(shader:  u32)                                                               #foreign ogl "glDeleteShader";
link_program::    proc(program: u32)                                                               #foreign ogl "glLinkProgram";
use_program::     proc(program: u32)                                                               #foreign ogl "glUseProgram";
delete_program::  proc(program: u32)                                                               #foreign ogl "glDeleteProgram";


get_shaderiv::         proc(shader:  u32, pname: i32, params: ^i32)                                #foreign ogl "glGetShaderiv";
get_programiv::        proc(program: u32, pname: i32, params: ^i32)                                #foreign ogl "glGetProgramiv";
get_shader_info_log::  proc(shader:  u32, max_length: u32, length: ^u32, info_long: ^byte)         #foreign ogl "glGetShaderInfoLog";
get_program_info_log:: proc(program: u32, max_length: u32, length: ^u32, info_long: ^byte)         #foreign ogl "glGetProgramInfoLog";

active_texture::   proc(texture: i32)                                                              #foreign ogl "glActiveTexture";
generate_mipmap::  proc(target:  i32)                                                              #foreign ogl "glGenerateMipmap";

sampler_parameteri::     proc(sampler: u32, pname: i32, param: i32)                                #foreign ogl "glSamplerParameteri";
sampler_parameterf::     proc(sampler: u32, pname: i32, param: f32)                                #foreign ogl "glSamplerParameterf";
sampler_parameteriv::    proc(sampler: u32, pname: i32, params: ^i32)                              #foreign ogl "glSamplerParameteriv";
sampler_parameterfv::    proc(sampler: u32, pname: i32, params: ^f32)                              #foreign ogl "glSamplerParameterfv";
sampler_parameter_iiv::  proc(sampler: u32, pname: i32, params: ^i32)                              #foreign ogl "glSamplerParameterIiv";
sampler_parameter_iuiv:: proc(sampler: u32, pname: i32, params: ^u32)                              #foreign ogl "glSamplerParameterIuiv";


uniform1i::         proc(loc: i32, v0: i32)                                                        #foreign ogl "glUniform1i";
uniform2i::         proc(loc: i32, v0, v1: i32)                                                    #foreign ogl "glUniform2i";
uniform3i::         proc(loc: i32, v0, v1, v2: i32)                                                #foreign ogl "glUniform3i";
uniform4i::         proc(loc: i32, v0, v1, v2, v3: i32)                                            #foreign ogl "glUniform4i";
uniform1f::         proc(loc: i32, v0: f32)                                                        #foreign ogl "glUniform1f";
uniform2f::         proc(loc: i32, v0, v1: f32)                                                    #foreign ogl "glUniform2f";
uniform3f::         proc(loc: i32, v0, v1, v2: f32)                                                #foreign ogl "glUniform3f";
uniform4f::         proc(loc: i32, v0, v1, v2, v3: f32)                                            #foreign ogl "glUniform4f";
uniform_matrix4fv:: proc(loc: i32, count: u32, transpose: i32, value: ^f32)                        #foreign ogl "glUniformMatrix4fv";

get_uniform_location::  proc(program: u32, name: ^byte) -> i32                                     #foreign ogl "glGetUniformLocation";
