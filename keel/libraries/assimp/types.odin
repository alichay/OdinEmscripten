#load "../../math/types.odin";
#import "raw.odin";

Plane :: struct #ordered {
	a,b,c,d: f32
}

Ray :: struct #ordered {
	pos, dir : Vec3
}

Color3d :: struct #ordered {
	r, g, b: f32
}

Color4d :: struct #ordered {
	r, g, b, a: f32
}

AssimpString :: struct #ordered {
	count: u64,
	data: ^byte
}

to_assimp_string :: proc(str: string) -> AssimpString #inline {
	return AssimpString{cast(u64)len(str), ^str[0]};
}
from_assimp_string :: proc(str: AssimpString) -> string #inline {
	ret: string;
	_ret:= cast(^raw.String)^ret;
	_ret.data = str.data;
	_ret.len = cast(int)str.count;
	return ret;
}

Return :: enum i32 {
	Success = 0x0,
	Failure = -0x1,
	OutOfMemory = -0x3,
}

MemoryInfo :: struct #ordered {
	textures:   uint,
	materials:  uint,
	meshes:     uint,
	nodes:      uint,
	animations: uint,
	cameras:    uint,
	lights:     uint,
	total:      uint,
}