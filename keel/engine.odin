#import "os.odin";
#import "fmt.odin";
#import "utils.odin";

#import "libraries/sdl.odin";
#import "libraries/gl.odin";
#import "graphics/shader.odin";
#import "graphics/mesh.odin";

#import am "libraries/assimp/types.odin";
#import "math.odin";

EMSCRIPTEN :: false;

running := true;

// Relevant to FPS
FPS_SAMPLES :: 10;
tick_index : u32 = 0;
tick_sum : u32 = 0;
tick_list : [FPS_SAMPLES] u32;
fps : f32;


begin :: proc() {

	//v: math.Mat3;
	//v[0][0] = 10;
	//v[0][1] = 20;
	//#no_bounds_check
	//v[1][0] = 30;
	// a1 = 10, a2 = 20, a3 = 0, b1 = 0, b2 = 0, b3 = 30, c1 = 0, c2 = 0, c3 = 0
	//fmt.println(v);
	//fmt.println(cast(am.Mat3)v);

	fmt.println("-- STARTING KEEL ENGINE --");

	if(sdl.init(sdl.INIT_EVERYTHING) < 0) {
		fmt.println("Failed to initialize SDL.");
		os.exit(1);
	}


	when(!EMSCRIPTEN) {

		sdl.gl_set_attribute(sdl.GlAttr.CONTEXT_MAJOR_VERSION, 3);
		sdl.gl_set_attribute(sdl.GlAttr.CONTEXT_MINOR_VERSION, 3);
		sdl.gl_set_attribute(sdl.GlAttr.CONTEXT_PROFILE_MASK, cast(int)sdl.GlProfile.CONTEXT_PROFILE_CORE);

		window, window_err := sdl.create_window("tester", sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, 800, 600, sdl.WINDOW_SHOWN | sdl.WINDOW_OPENGL);

		if(window_err) {
			fmt.println("Failed to create a window.");
			os.exit(2);
		};

	} else {

		sdl.gl_set_attribute(sdl.GlAttr.STENCIL_SIZE, 8);
		sdl.gl_set_attribute(sdl.GlAttr.DEPTH_SIZE, 24);
		sdl.gl_set_attribute(sdl.GlAttr.DOUBLEBUFFER, 1);
		sdl.gl_set_attribute(sdl.GlAttr.CONTEXT_MAJOR_VERSION, 2);
		sdl.gl_set_attribute(sdl.GlAttr.CONTEXT_MINOR_VERSION, 2);

		window, window_err := sdl.create_window("tester", sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, 800, 600, sdl.WINDOW_RESIZABLE | sdl.WINDOW_OPENGL);

		if(window_err) {
			fmt.println("Failed to create a canvas.");
			os.exit(2);
		};

	}

	gl_context, context_err := sdl.gl_create_context(window);

	if(context_err) {
		fmt.println("Failed to create an OpenGL context.");
		os.exit(3);
	}

	defer sdl.gl_delete_context(gl_context);

	sdl.make_context_current(window, gl_context);

	gl.init();

	gl.enable(gl.DEPTH_TEST);

	gl.depth_func(gl.LESS);

	gl.enable(gl.CULL_FACE);

	// TODO(zachary): WHAT DOES THIS DO????
	vertex_array_id : u32;
	gl.gen_vertex_arrays(1, ^vertex_array_id);
	gl.bind_vertex_array(vertex_array_id);

	gl.clear_color(0, 0, 0.4, 1);

	sdl.set_relative_mouse_mode(true);
	defer sdl.set_relative_mouse_mode(false);

	test_vertices := [dynamic]f32{-1, -1, 0, 1, -1, 0, 0, 1, 0};
	test_indices  := [dynamic]u16{0,1,2};
	defer(free(test_vertices));
	defer(free(test_indices));

	m := mesh.create(test_vertices, test_indices, "Test Mesh");

	test_shader, shader_success := shader.load_shader("default");
	if(!shader_success) {
		fmt.println("Failed to load shader.");
		os.exit(1);
	}

	last_time: u32 = sdl.get_ticks();

	for running {

		// Calculate delta time and FPS
		delta: u32;
		{
			now := sdl.get_ticks();
			// Calculate DT
			delta = cast(u32)(now - last_time);
			// Calculate FPS
			tick_sum -= tick_list[tick_index];
			tick_sum += delta;
			tick_list[tick_index] = delta;
			tick_index += 1;
			if(tick_index == FPS_SAMPLES) {
				tick_index = 0;
			}
			fps = 1000.0 / (cast(f32)tick_sum / cast(f32)FPS_SAMPLES);
			// Reset timing
			last_time = now;
		}
		fmt.printf("FPS: %v\r", fps);

		// Events
		event: sdl.Event;
		for sdl.poll_event(^event) != 0 {
			match event.type_id {
				case cast(u32)sdl.EventType.QUIT: {
					running = false;
				}
				case cast(u32)sdl.EventType.KEYDOWN: {
					if(event.key.keysym.sym == sdl.KEY_ESCAPE) {
						sdl.set_relative_mouse_mode(false);
					}
					fmt.println("kup:   ", sdl.get_scancode_name(event.key.keysym.scancode), "\t", event.key);

				}
				case cast(u32)sdl.EventType.KEYUP: {
					fmt.println("kdown: ", sdl.get_scancode_name(event.key.keysym.scancode), "\t", event.key);
				}
				case cast(u32)sdl.EventType.MOUSEBUTTONDOWN: {
					sdl.set_relative_mouse_mode(true);
					fmt.println("m down:", event.button);
				}
				case cast(u32)sdl.EventType.MOUSEBUTTONUP: {
					fmt.println("m up:  ", event.button);
				}
				case cast(u32)sdl.EventType.MOUSEWHEEL: {
					fmt.println("wheel: ", event.wheel);
				}
				case cast(u32)sdl.EventType.MOUSEMOTION: {
					//fmt.println("m move:", event.motion);
				}
				default: {
					fmt.println(event.type_id);
				}
			}
		}
		// Clear the screen
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		// Render the scene
		shader.bind_shader(test_shader);

		mesh.draw(m);

		shader.unbind_shaders();

		// Flush buffer to window
		sdl.gl_swap_window(window);

		// Update entities

		// Loop again
		//fmt.println(sdl.get_mouse_state());
		sdl.sleep(00);
	}
}
