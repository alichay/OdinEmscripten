#import "../../libraries/gl.odin";
#import "../../utils.odin";
#import "shader.odin";

Mesh :: struct {
	vertex_buffer, element_buffer: u32,
	indices_count: u32,
	name: string
}

create :: proc(vertices: [dynamic]f32, indices: [dynamic]u16, name: string) -> Mesh {

	vertex_buffer : u32;
	gl.gen_buffers(1, ^vertex_buffer);
	gl.bind_buffer(gl.ARRAY_BUFFER, vertex_buffer);
	gl.buffer_data(gl.ARRAY_BUFFER, len(vertices) * size_of(f32), ^vertices[0], gl.STATIC_DRAW);

	element_buffer : u32;
	gl.gen_buffers(1, ^element_buffer);
	gl.bind_buffer(gl.ELEMENT_ARRAY_BUFFER, element_buffer);
	gl.buffer_data(gl.ELEMENT_ARRAY_BUFFER, len(indices) * size_of(u16), ^indices[0], gl.STATIC_DRAW);

	return Mesh{vertex_buffer, element_buffer, cast(u32)len(indices), name};
}

draw :: proc(m: Mesh) #inline {

	gl.enable_vertex_attrib_array(0);

	gl.vertex_attrib_pointer(0, 3, gl.FLOAT, gl.FALSE, 0, nil);
	gl.bind_buffer(gl.ELEMENT_ARRAY_BUFFER, m.element_buffer);
	gl.draw_elements(gl.TRIANGLES, m.indices_count, gl.UNSIGNED_SHORT, nil);
	
	gl.disable_vertex_attrib_array(0);
}