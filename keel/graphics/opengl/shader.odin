#import "os.odin";
#import "fmt.odin";
#import "strings.odin";
#import "raw.odin";
#import "../../utils.odin";
#import "../../libraries/gl.odin";


// TODO(zachary): Make a function that takes a shader AST and creates a GLSL shader out of it.

Shader :: struct {
	program_id: u32,
	name: string,
	properties: ShaderProperties,
}

ShaderProperties :: struct {

}

load_shader :: proc(name: string) -> (Shader, bool) {

	// Create a buffer on the GPU
	vertex_shader_id := gl.create_shader(gl.VERTEX_SHADER);
	fragment_shader_id := gl.create_shader(gl.FRAGMENT_SHADER);

	_buf1, vertex_shader_path := utils.cat("./res/shaders/", name, "/vert.glsl");
	_buf2, fragment_shader_path := utils.cat("./res/shaders/", name, "/frag.glsl");
	defer free(_buf1);
	defer free(_buf2);

	// Load shader sources into strings
	vertex_c_str, success_vert := os.read_entire_file(vertex_shader_path);
	fragment_c_str, success_frag := os.read_entire_file(fragment_shader_path);
	defer(free(vertex_c_str));
	defer(free(fragment_c_str));
		
	if(!success_vert) {
		fmt.printf("Failed to load vertex shader \"%v\"\n", vertex_shader_path);
	}
	if(!success_vert) {
		fmt.printf("Failed to load vertex shader \"%v\"\n", fragment_shader_path);
	}
	if(!success_vert || !success_frag) {
		return Shader{}, false;
	}

	fmt.println(strings.to_odin_string(^fragment_c_str[0]));

	info_log_length : u32 = 0;
	success := true;

	// Compiling vertex shader
	gl.shader_source(vertex_shader_id, 1, cast(^^byte)(^(cast(^raw.Slice)^vertex_c_str).data), nil);
	gl.compile_shader(vertex_shader_id);

	// Check vertex shader
	gl.get_shaderiv(vertex_shader_id, gl.INFO_LOG_LENGTH, cast(^i32)^info_log_length);
	if(info_log_length > 0) {
		log: [dynamic]u8;
		reserve(log, info_log_length+1);
		gl.get_shader_info_log(vertex_shader_id, info_log_length, nil, ^log[0]);
		fmt.print(vertex_shader_path);
		fmt.println(strings.to_odin_string(^log[0]));
		free(log);
		success = false;
	}

	// Compiling fragment shader
	gl.shader_source(fragment_shader_id, 1, cast(^^byte)(^(cast(^raw.Slice)^fragment_c_str).data), nil);
	gl.compile_shader(fragment_shader_id);

	// Check fragment shader
	gl.get_shaderiv(fragment_shader_id, gl.INFO_LOG_LENGTH, cast(^i32)^info_log_length);
	if(info_log_length > 0) {
		log: [dynamic]u8;
		reserve(log, info_log_length+1);
		gl.get_shader_info_log(fragment_shader_id, info_log_length, nil, ^log[0]);
		fmt.print(fragment_shader_path);
		fmt.println(strings.to_odin_string(^log[0]));
		free(log);
		success = false;
	}

	if(!success) {
		return Shader{}, false;
	}
	
	// Link program
	program_id := gl.create_program();
	gl.attach_shader(program_id, vertex_shader_id);
	gl.attach_shader(program_id, fragment_shader_id);
	gl.link_program(program_id);

	// Check the program
	gl.get_programiv(fragment_shader_id, gl.INFO_LOG_LENGTH, cast(^i32)^info_log_length);
	if(info_log_length > 0) {
		log: [dynamic]u8;
		reserve(log, info_log_length+1);
		gl.get_program_info_log(program_id, info_log_length, nil, ^log[0]);
		fmt.println(strings.to_odin_string(^log[0]));
		free(log);
		
		return Shader{}, false;
	}

	gl.detach_shader(program_id, vertex_shader_id);
	gl.detach_shader(program_id, fragment_shader_id);

	gl.delete_shader(vertex_shader_id);
	gl.delete_shader(fragment_shader_id);

	return Shader{program_id, name, ShaderProperties{}}, true;
}

bind_shader :: proc(shader: Shader) #inline {
	gl.use_program(shader.program_id);
}

unbind_shaders :: proc() #inline {
	gl.use_program(0);
}
