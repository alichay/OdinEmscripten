#import "shader.odin";

PropertyType :: enum {
	INT32, INT16, UINT32, UINT16, F32, F64
}

Property :: struct {
	id: u32,
	name: string,
	value: union {
		
	}
}

Material :: struct {
	shader: shader.Shader;

}