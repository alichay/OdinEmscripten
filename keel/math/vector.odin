#load "types.odin";
#import "math.odin";

add :: proc(this, o: math.Vec3) -> math.Vec3 #inline {
	return this + o;
}
add :: proc(this: math.Vec3, amt: f32) -> math.Vec3 #inline {
	return this + amt;
//	return math.Vec3{this.x + amt, this.y + amt, this.z + amt};
}
sub :: proc(this, o: math.Vec3) -> math.Vec3 #inline {
	return this - o;
}
sub :: proc(this: math.Vec3, amt: f32) -> math.Vec3 #inline {
	return math.Vec3{this.x - amt, this.y - amt, this.z - amt};
}
mul :: proc(this: math.Vec3, mat: Mat4) -> math.Vec3 #inline {
	res: math.Vec3;
	res.x = mat.a1 * this.x + mat.a2 * this.y + mat.a3 * this.z + mat.a4;
	res.y = mat.b1 * this.x + mat.b2 * this.y + mat.b3 * this.z + mat.b4;
	res.z = mat.c1 * this.x + mat.c2 * this.y + mat.c3 * this.z + mat.c4;
	return res;
}
mul :: proc(this: math.Vec3, mat: Mat3) -> math.Vec3 #inline {
	res: math.Vec3;
	res.x = mat.a1 * this.x + mat.a2 * this.y + mat.a3 * this.z;
	res.y = mat.b1 * this.x + mat.b2 * this.y + mat.b3 * this.z;
	res.z = mat.c1 * this.x + mat.c2 * this.y + mat.c3 * this.z;
	return res;
}
mul :: proc(this, o: math.Vec3) -> math.Vec3 #inline {
	return math.Vec3{this.x * o.x, this.y * o.y, this.z * o.z};
}
mul :: proc(this: math.Vec3, amt: f32) -> math.Vec3 #inline {
	res: math.Vec3;
	res.x = this.x * amt;
	res.y = this.y * amt;
	res.z = this.z * amt;
	return res;
}
quo :: proc(this: math.Vec3, amt: f32) -> math.Vec3 #inline {
	res: math.Vec3;
	res.x = this.x / amt;
	res.y = this.y / amt;
	res.z = this.z / amt;
	return res;
}
quo :: proc(this, o: math.Vec3) -> math.Vec3 #inline {
	return math.Vec3{this.x / o.x, this.y / o.y, this.z / o.z};
}
length :: proc(this: math.Vec3) -> f32 #inline {
	return this.x * this.x + this.y * this.y + this.z * this.z;
}
normalize :: proc(this: math.Vec3) -> math.Vec3 #inline {
	return mul(this, 1/length(this));
}
normalize :: proc(this: ^math.Vec3) #inline {
	len := length(this^);
	this.x /= len;
	this.y /= len;
	this.z /= len;
}
cmp :: proc(a, b: math.Vec3) -> bool #inline {
	return a.x == b.x && a.y == b.y && a.z == b.z;
}
cmp :: proc(a, b: math.Vec3, epsilon: f32) -> bool #inline {

}