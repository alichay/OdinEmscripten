Vec2 :: struct #ordered {
	x, y: f32
}

Vec3 :: struct #ordered {
	x, y, z: f32
}

Mat4 :: struct #ordered {
	a1, a2, a3, a4: f32,
	b1, b2, b3, b4: f32,
	c1, c2, c3, c4: f32,
	d1, d2, d3, d4: f32,
}

Mat3 :: struct #ordered {
	a1, a2, a3: f32,
	b1, b2, b3: f32,
	c1, c2, c3: f32,
}

Quat :: struct #ordered {
	w, x, y, z: f32
}
