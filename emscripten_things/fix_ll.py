#!/bin/env python2.7

import sys
read_ll = open(sys.argv[1], 'r')
ll = read_ll.read()
read_ll.close()

ll = ll.replace("declare ccc void @llvm.debugtrap()", "declare ccc void @___js_alert()").replace("@llvm.debugtrap", "@___js_alert")
ll = ll.replace("""declare ccc %..rawptr @malloc(i64) 
declare ccc void @free(%..rawptr) 
declare ccc %..rawptr @realloc(%..rawptr, i64)""", "")
ll = ll.replace("""declare ccc %..rawptr @HeapAlloc(%..rawptr, i32, i64) 
declare ccc %..rawptr @HeapReAlloc(%..rawptr, i32, %..rawptr, i64) 
declare ccc i32 @HeapFree(%..rawptr, i32, %..rawptr) 
declare ccc %..rawptr @GetProcessHeap()""", "")

ll = ll.replace("declare ccc void @llvm.assume(i1)",
"""
declare ccc %..rawptr @malloc(i64) 
declare ccc void @free(%..rawptr) 
declare ccc %..rawptr @realloc(%..rawptr, i64)


define ccc i8* @HeapAlloc(%..rawptr %h, i32 %flags, i64 %bytes) {
decls-0:
	%0 = alloca %..rawptr, align 8
	%1 = alloca i32, align 4
	%2 = alloca i64, align 8
	store %..rawptr zeroinitializer, %..rawptr* %0
		store %..rawptr %h, %..rawptr* %0
	store i32 zeroinitializer, i32* %1
		store i32 %flags, i32* %1
	store i64 zeroinitializer, i64* %2
		store i64 %bytes, i64* %2
	%3 = load i64, i64* %2, align 8
	%4 = call ccc %..rawptr @malloc(i64 %3)
	ret %..rawptr %4
}

define ccc %..rawptr @HeapReAlloc(%..rawptr %h, i32 %flags, %..rawptr %memory, i64 %bytes) {
decls-0:
	%0 = alloca %..rawptr, align 8
	%1 = alloca i32, align 4
	%2 = alloca %..rawptr, align 8
	%3 = alloca i64, align 8
	store %..rawptr zeroinitializer, %..rawptr* %0
		store %..rawptr %h, %..rawptr* %0
	store i32 zeroinitializer, i32* %1
		store i32 %flags, i32* %1
	store %..rawptr zeroinitializer, %..rawptr* %2
		store %..rawptr %memory, %..rawptr* %2
	store i64 zeroinitializer, i64* %3
		store i64 %bytes, i64* %3
	%4 = load %..rawptr, %..rawptr* %2, align 8
	%5 = load i64, i64* %3, align 8
	%6 = call ccc %..rawptr @realloc(%..rawptr %4, i64 %5)
	ret %..rawptr %6
}

define ccc i32 @HeapFree(%..rawptr %h, i32 %flags, %..rawptr %memory) {
decls-0:
	%0 = alloca %..rawptr, align 8
	%1 = alloca i32, align 4
	%2 = alloca %..rawptr, align 8
	store %..rawptr zeroinitializer, %..rawptr* %0
		store %..rawptr %h, %..rawptr* %0
	store i32 zeroinitializer, i32* %1
		store i32 %flags, i32* %1
	store %..rawptr zeroinitializer, %..rawptr* %2
		store %..rawptr %memory, %..rawptr* %2
	%3 = load %..rawptr, %..rawptr* %2, align 8
	call ccc void @free(%..rawptr %3)
	ret i32 1
}

define ccc %..rawptr @GetProcessHeap() {
decls-0:
	%0 = alloca i64, align 8
	store i64 1, i64* %0
	%1 = bitcast i64* %0 to %..rawptr
	ret %..rawptr %1
}



declare ccc void @llvm.assume(i1) """)


write_ll = open(sys.argv[1], 'w')
write_ll.write(ll)
write_ll.close()